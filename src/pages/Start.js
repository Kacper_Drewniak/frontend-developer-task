import React, {useState} from "react";
import {Formik, Field, Form} from 'formik';
import {useSelector, useDispatch} from 'react-redux'
import {updateNickname} from "../features/player/playerSlice";
import {Container, Label, Button} from "reactstrap";
import * as Yup from 'yup';
import {Link} from "react-router-dom";

const SignupSchema = Yup.object().shape({
    nickname: Yup.string()
        .min(2, 'Too Short!')
        .max(10, 'Too Long!')
        .required('Required'),
});


const Start = () => {

    const nickname = useSelector((state) => state.player.nickname)
    const dispatch = useDispatch()

    const [isToggle, setIsToggle] = useState(true)

    const toggleView = () => {
        setIsToggle(!isToggle);
    }


    return <Container className="start__container">
        <h1 className="start__container--title">{nickname ? `Hello ${nickname}` : `enter your name:`}</h1>
        {isToggle ? <Formik
            initialValues={{
                nickname: null,
            }}
            validationSchema={SignupSchema}
            onSubmit={async (values) => {
                dispatch(updateNickname(String(values.nickname)));
                toggleView();
            }}
        >
            {({errors, touched}) => <Form className="start__container__form">
                <Label className="start__container__form--nickname"
                       htmlFor="nickname">nickname</Label>
                <Field id="nickname" name="nickname" placeholder="Jane"/>
                {errors.nickname && touched.nickname ? <div>{errors.nickname}</div> : null}
                <Button type="submit">Submit</Button>
            </Form>}
        </Formik> : <>
            <h1>Start Game</h1>
            <Button className="formik__container--button" onClick={toggleView}>Change nickname</Button>
            <Link to="/game"><Button>Start Game!</Button></Link>
        </>}
    </Container>
}


export default Start;