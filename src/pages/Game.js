import React, {useState, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import images from "../data/images";
import {shuffleArray, divideByTwo} from "../helpers/helpers";
import {
    CardImg,
    Row, Col, NavItem
} from 'reactstrap';
import ReactCardFlip from 'react-card-flip';
import {addRound} from "../features/player/playerSlice";
import {Button, Modal, ModalHeader, ModalBody} from 'reactstrap';
import {Link} from "react-router-dom";

const Game = () => {

    const dispatch = useDispatch()

    const nickname = useSelector(({player}) => player.nickname)

    const [cards, setCards] = useState(shuffleArray(images));
    const [activeCards, setActiveCards] = useState([]);
    const [quantityTries, setQuantityTries] = useState(0)
    const [seconds, setSeconds] = useState(0);
    const [timerIsActive, timerSetIsActive] = useState(false);
    const [isGameStopped, setIsGameStopped] = useState(false);
    const [score, setScore] = useState()
    const [isGameStarted, setIsGameStarted] = useState(false);

    const hiddenAllCard = (cardArray) => {
        cardArray.forEach(card => card.hidden = true);
    }

    const startGame = () => {
        setIsGameStarted(true)
        setIsGameStopped(false)
        timerSetIsActive(true)
    }

    const stopGame = () => {
        setScore(activeCards.length / 2)
        setActiveCards([]);
        setCards(shuffleArray(images));
        hiddenAllCard(cards);
        timerSetIsActive(false);
        setIsGameStopped(true);
    }

    const addTry = () => {
        setQuantityTries(quantityTries + 1)
    }

    const resetGame = () => {
        setQuantityTries(0);
        setSeconds(0);
        startGame();
    }

    const handleTry = (id) => {
        const card = cards.find(c => c.id === id)
        card.hidden = !card.hidden

        const openCards = cards.filter(c => c.hidden === false && !activeCards.find(active => active.id === c.id))
        if (openCards.length <= 2) setCards([...cards])
        if (openCards.length === 2) {
            if (openCards[0].key === openCards[1].key)
                setActiveCards([...activeCards, ...openCards]);
            else hiddenCards();
            addTry();
        }
    }

    const hiddenCards = () => {
        setTimeout(() => {
            cards.forEach(c => {
                if (!activeCards.find(a => a.id === c.id))
                    c.hidden = true
            });
            setCards([...cards])
        }, 2000)
    }


    useEffect(() => {
        let interval = null;
        if (timerIsActive) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds + 1);
            }, 1000);
        } else if (!timerIsActive && seconds !== 0) {
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [timerIsActive, seconds]);

    if (activeCards.length === cards.length) {
        dispatch(addRound({
            quantityTries: quantityTries,
            score: divideByTwo(activeCards.length),
            time: seconds
        }))
        stopGame();
    }

    return <>
        {!isGameStarted && <Modal isOpen>
            <ModalBody>
                <h1>Hello {nickname} !</h1>
                <Button onClick={startGame}>Start Game</Button>
            </ModalBody>
        </Modal>}
        {isGameStopped && <Modal isOpen>
            <div>
                <Modal isOpen>
                    <ModalBody>
                        <ModalHeader>
                            <NavItem>
                                <Link to="/">Start</Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/score">Score</Link>
                            </NavItem>
                        </ModalHeader>
                        <p>Game has stopped</p>
                        <h1>Time {seconds}s</h1>
                        <p>player: {nickname}</p>

                        <h1>points: {score}</h1>
                        <h2>tries: {quantityTries}</h2>

                        <Button onClick={resetGame}>start new game</Button>
                    </ModalBody>
                </Modal>
            </div>
        </Modal>}


        <h1>Time {seconds}s</h1>
        <p>player: {nickname}</p>
        <h1>points: {divideByTwo(activeCards.length)}</h1>
        <h2>tries: {quantityTries}</h2>
        <Row>
            {cards.map((card, index) => {
                return <Col
                    xs="3"
                >
                    <ReactCardFlip isFlipped={card.hidden} flipDirection="vertical">
                        <CardImg className="card__container--image" top width="100%" src={card.url}/>
                        <CardImg className="card__container--image" onClick={() => handleTry(card.id)}
                                 top
                                 width="100%"
                                 src="https://cdn.pixabay.com/photo/2014/05/21/19/14/the-question-mark-350168_960_720.png"/>
                    </ReactCardFlip>
                </Col>
            })}
        </Row>
    </>
}


export default Game;