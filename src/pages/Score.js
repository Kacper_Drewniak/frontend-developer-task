import React from "react";
import {useSelector} from "react-redux";
import {Table} from 'reactstrap';

const Score = () => {
    const rounds = useSelector((state) => state.player.rounds)

    return <Table>
        <thead>
        <tr>
            <th>#</th>
            <th>Score</th>
            <th>Quantity Tries</th>
            <th>Time</th>
        </tr>
        </thead>
        <tbody>
        {rounds.map(({score, quantityTries, time}, i) => {
            return <tr>
                <th scope="row">{i + 1}</th>
                <td>{score}</td>
                <td>{quantityTries}</td>
                <td>{time} s</td>
            </tr>
        })}

        </tbody>
    </Table>
}


export default Score;