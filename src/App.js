import React from "react";
import "./styles/index.scss";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Game from "./pages/Game";
import Score from "./pages/Score";
import Start from "./pages/Start";
import Navbar from "./components/Navbar";

const App = () => <Router>
    <Navbar/>
    <Switch>
        <Route path="/score">
            <Score/>
        </Route>
        <Route path="/game">
            <Game/>
        </Route>
        <Route path="/">
            <Start/>
        </Route>
    </Switch>
</Router>


export default App;
