export const multiplyArray = (array) => {
    return [...array, ...array];
}

export const shuffleArray = (array) => {
    return array.sort(() => Math.random() - 0.5);
}
export const divideByTwo = (number) => {
    return number / 2;
}
