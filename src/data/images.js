const images = [
    {
        id: 1,
        key: 1,
        url: "https://cdn.pixabay.com/photo/2021/06/29/18/55/mountain-slope-6374980_960_720.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 2,
        key: 2,
        url: "https://cdn.pixabay.com/photo/2021/04/12/21/46/girls-6174061__340.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 3,
        key: 3,
        url: "https://cdn.pixabay.com/photo/2021/05/18/08/07/buildings-6262595__340.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 4,
        key: 4,
        url: "https://cdn.pixabay.com/photo/2021/05/23/21/57/mountains-6277391__340.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 5,
        key: 5,
        url: "https://cdn.pixabay.com/photo/2019/07/21/07/12/new-york-4352072__340.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 6,
        key: 6,
        url: "https://cdn.pixabay.com/photo/2021/02/24/23/43/boy-6047786__340.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 7,
        key: 1,
        url: "https://cdn.pixabay.com/photo/2021/06/29/18/55/mountain-slope-6374980_960_720.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 8,
        key: 2,
        url: "https://cdn.pixabay.com/photo/2021/04/12/21/46/girls-6174061__340.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 9,
        key: 3,
        url: "https://cdn.pixabay.com/photo/2021/05/18/08/07/buildings-6262595__340.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 10,
        key: 4,
        url: "https://cdn.pixabay.com/photo/2021/05/23/21/57/mountains-6277391__340.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 11,
        key: 5,
        url: "https://cdn.pixabay.com/photo/2019/07/21/07/12/new-york-4352072__340.jpg",
        title: "green",
        hidden: true
    },
    {
        id: 12,
        key: 6,
        url: "https://cdn.pixabay.com/photo/2021/02/24/23/43/boy-6047786__340.jpg",
        title: "green",
        hidden: true
    },
]


export default images;