import {Nav, NavItem} from "reactstrap";
import {Link} from "react-router-dom";
import React from "react";

const Navbar = () => {
    const links = [{
        url: "/",
        label: "Start"
    }, {
        url: "/game",
        label: "Game"
    }, {
        url: "/score",
        label: "Score"
    },
    ]
    return <Nav className="navbar__container">
        {links.map(({url, label}) =>
            <NavItem className="navbar__container--item">
                <Link class="navbar__container--href"
                      to={url}>
                    {label}</Link>
            </NavItem>
        )}
    </Nav>
}


export default Navbar;