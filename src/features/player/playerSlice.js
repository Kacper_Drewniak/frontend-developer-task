import {createSlice} from '@reduxjs/toolkit'

export const playerSlice = createSlice({
    name: 'player',
    initialState: {
        nickname: "",
        rounds: []
    },
    reducers: {
        updateNickname: (state, action) => {
            state.nickname = action.payload
        },
        addRound: (state, action) => {
            state.rounds = [...state.rounds, action.payload]
        }
    },
})

export const {updateNickname, addRound} = playerSlice.actions

export default playerSlice.reducer